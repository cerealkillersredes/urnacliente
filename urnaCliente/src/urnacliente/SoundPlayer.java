/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnacliente;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author ricardo
 */
public class SoundPlayer implements Runnable {
    private final File audioFile;
    private final AudioInputStream audioStream;
    private final AudioFormat formato;
    private final DataLine.Info info;
    private final Clip clip;

    public SoundPlayer() throws Exception {
        try {
            this.audioFile = new File("sucesso.wav");
            this.audioStream = AudioSystem.getAudioInputStream(this.audioFile);
            this.formato = audioStream.getFormat();
            this.info = new DataLine.Info(Clip.class, formato);
            this.clip = (Clip) AudioSystem.getLine(info);
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            throw new Exception();
        }
    }
    
    @Override
    public void run() {
        try {
            this.clip.open(this.audioStream);
            this.clip.start();

            Thread.sleep(clip.getMicrosecondLength()/1000);

            this.clip.close();
            this.audioStream.close();
    
            
        } catch (LineUnavailableException | IOException | InterruptedException e) {
        }
    }
    
}
