/*
 * Nomes(NºUSP) :
 * 
 * Fabrício Pereira de Freitas (8910372)
 * Pedro Carrascosa C. Dias (7173480)
 * Ricardo Chagas (8957242)
 */
package urnacliente;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

/**
 *
 * @author ricardo
 */
public class UrnaCliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ObjectInputStream input;
        ObjectOutputStream output;
        Socket socket;
        Thread gui;
        InetAddress address;
        
        try {
            address = InetAddress.getByName(new URL("https://cosmos.lasdpc.icmc.usp.br").getHost());
            socket = new Socket(address, 40109);
        } catch (Exception e) {
            System.out.println("Nao foi possivel estabelecer conexao com o servidor");
            return;
        }
        
        try {
            output = new ObjectOutputStream(socket.getOutputStream());            
            input = new ObjectInputStream(socket.getInputStream());
        } catch (Exception e) {
            System.out.println("Houve um erro de conexao");
            return;
        }
        
        
        gui = new Thread(new JanelaVotacao(output, input));
        gui.start();
    }
}
